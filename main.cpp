#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>
using namespace std;


vector< vector<int> > fullArray;

bool validNum(int num, vector<int> row)
{
    bool validRow = true;
    bool validCol = true;
    // Validate the rows
    /*
    if (!row.empty())
    {
        cout << "HELLO\n";
        for (int i = 0; i < row.size(); i++)
        {
            if(num == row[i])
                return false; // number already in row
        }
    }
    */
    // Validate the columns  

    if (!fullArray.empty())
    {
        for (int j = fullArray.size()-1; j >= 0; j--) // validate col
        {
            //cout << "Row: " << j << " Col: " << row.size()-1 << endl;
            if (num == fullArray[j][row.size()])
                return false; // number already in column
        }
    }
    return true;
}

void buildPuzzle()
{
    srand (time(NULL));
    int x;
    while (fullArray.size() != 3)
    {
        vector<int> row;
        while (row.size() != 3)
        {
            //cout << "RowSize: " << row.size() << endl;
            x = rand() % 3 + 1;
            //cout << "Valid: " << validNum(x,row) << endl;
            if (validNum(x,row))
                row.push_back(x); 
        }
        fullArray.push_back(row);
    }
}

void printPuzzle()
{
    for (int i = 0; i < fullArray.size(); i++)
    {
        for (int j = 0; j < fullArray[i].size(); j++)
        {
            cout << fullArray[i][j] << " ";
        }
    cout << endl;
    }
}

int main()
{
    buildPuzzle();
    printPuzzle();
    return 0;
}

